﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Earthquake
{
    public class DataGridShowInfo
    {
        public string Magnitude { get; set; } = "";
        public string Depth { get; set; } = "";
        public string Place { get; set; } = "";
        public DateTime? Date { get; set; }
    }
}
