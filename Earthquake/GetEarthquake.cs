﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Earthquake
{
    public class GetEarthquake
    {
        public EarthquakeInformation GetQuakeFunction(int number = 1)
        {
            using (WebClient client = new WebClient())
            {
                
                //if(number == "")
                //{
                //    var jsonString = client.DownloadString($"https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&limit=10");
                //    var quakeInformation = EarthquakeInformation.FromJson(jsonString);
                //    return quakeInformation;
                //}
                //else
                //{
                    var jsonString = client.DownloadString($"https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&limit={number}");
                    var quakeInformation = EarthquakeInformation.FromJson(jsonString);
                    return quakeInformation;
                //}
            }
        }

        //public QuakeDetails GetQuakeDetails(string jsonString)
        //{
        //    var quakeDetails = QuakeDetails.FromJson(jsonString);
        //    //return quakeDetails.Properties.Products.Origin[0].Properties.Depth;
        //    return quakeDetails;
        //}

        //public List<DataGridShowInfo> DataGridQuakeShow(int number, QuakeDetails quakeDetails)
        //{
        //    var quake = GetQuakeFunction(number);
        //    var quakeProperties = quake.Features;

        //    var gridInfoList = new List<DataGridShowInfo>();
        //    foreach (var item in quakeProperties)
        //    {
        //        var dataGridShowInfo = new DataGridShowInfo();
        //        dataGridShowInfo.Depth = quakeDetails.Properties.Products.Origin[0].Properties.Depth.ToString();
        //        gridInfoList.Add(dataGridShowInfo);
        //    }
        //    return gridInfoList;
        //}

        public QuakeDetails GetQuakeDetailsFunction(string id)
        {
            using (WebClient client = new WebClient())
            {
                var jsonString = client.DownloadString($"https://earthquake.usgs.gov/fdsnws/event/1/query?eventid={id}&format=geojson");
                var quakeDetailsInformation = QuakeDetails.FromJson(jsonString);
                return quakeDetailsInformation;
            }
        }
    }
}
