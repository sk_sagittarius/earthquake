﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Earthquake
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void buttonClick(object sender, RoutedEventArgs e)
        {
            WebClient client = new WebClient();

            var stringToInt = textBox.Text;
            for (int i = 0; i < stringToInt.Length; i++)
            {
                if (stringToInt[i] >= '0' && stringToInt[i] <= '9')
                {
                    var getQuake = new GetEarthquake();

                    var textBoxText = textBox.Text;
                    string textBoxToString = textBoxText.ToString();
                    int textBoxtoInt = int.Parse(textBoxToString);
                    var jsonString = client.DownloadString($"https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&limit={textBoxtoInt}");
                    
                    //для вывода
                    //var myObject = JsonConvert.DeserializeObject<QuakeDetails>(jsonString);
                    //getQuake.DataGridQuakeShow(textBoxtoInt, myObject);
                    var quake = getQuake.GetQuakeFunction(textBoxtoInt);
                    
                    var quakeProperties = quake.Features;

                    

                    for (int j = 0; j < textBoxtoInt; j++)
                    {
                        var id = j + 1;

                        //var jsonStringToDetail = $"{quake.Features.ElementAt(j).Properties.Detail}";
                        //var quakeDepth = getQuake.GetQuakeDetails(jsonStringToDetail);
                        var quakePlace = quake.Features.ElementAt(j).Properties.Place;
                        var quakeDate = quakeProperties.ElementAt(j).Properties.Time;
                        var quakeId = quakeProperties.ElementAt(j).Id;
                        getQuake.GetQuakeDetailsFunction(quakeId);


                    }
                    
                    

                }
                else
                {
                    MessageBox.Show("Неправильный формат ввода");
                    break;
                }
            }


        }
    }
}
